import { BrowserRouter, Router, Routes, Route } from "react-router-dom";
import "antd/dist/antd.less";
import "./App.css";

// Import Screen Views
import Home from "./app/views/Home";

const App = () => (
  <BrowserRouter>
    <Router>
      <Routes>
        <Route exact path="/" component={Home} />
      </Routes>
    </Router>
  </BrowserRouter>
);

export default App;
